package com.example.securedev;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText email;
    private EditText password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.email_text_entry);
        password = findViewById(R.id.password_text_entry);
    }

    public void registerFromLogin(View view) {
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
    }

    public void loginOnClick(View view) {

        if (TextUtils.isEmpty(email.getText().toString())){
            //display error
            email.setError("Email is required");
            return;
        }
        if (TextUtils.isEmpty(password.getText().toString())){
            //display error
            email.setError("Email is required");
            return;
        }


        if (email.getText()!=null && password.getText()!=null){

            mAuth.signInWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "incorrect password or email", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }
}